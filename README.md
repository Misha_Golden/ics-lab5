## Before you start
### Code is awful 
⚠️ This application is not for production use, so please don't use any code snipped in your real projects. 

## Launch the app
### 1. Build docker image
```
docker build -t python-simple-app:v5
```

### 2. Run application
```
docker run -p 80:5000 -e ENVIRONMENT=prod -e db_user=<db_username> -e db_hostname=<db_hostname> -e db_password=<password>  python-simple-app:v3
```